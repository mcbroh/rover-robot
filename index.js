const Compass = require('./compass');
const ObstaclesChecker = require('./obstaclesChecker');
const Surrounding = require('./surrounding');
const Message = require('./Message');

class Robot extends ObstaclesChecker {
  constructor(gridSize, direction, position = { x: 0, y: 0 }, obstacles = { x: [], y: [] }) {
    super();
    // Grid size
    this.gridSize = gridSize;
    // create Robot obj
    this.robot = { direction, x: position['x'], y: position['y'] };
    // Add obstacles
    this.obstacles = { x: obstacles['x'], y: obstacles['y'] };
    // set out of bounds flag
    this.outOfBounds = false;
    // Travel logs
    this.visitedGrids = [];

    this.compass = new Compass();
    this.finalLocation = new Message();
    this.surrounding = new Surrounding(this.gridSize);

  }

  turn(face) {
    this.robot.direction = this.compass.getNewDirection(this.robot.direction, face);
  }

  move(facing) {
    if (this.surrounding.checkInBounds(this.robot, facing))
       this.robot = this.nextGrid(facing, this.robot)
    else {
      this.outOfBounds = true;
      return
    }
    this.visitedGrids.push(`${this.robot.x}, ${this.robot.y}`);
  }

  start(commands) {
      
      commands = commands.toLowerCase()
  
      // check for invalid commands
      const commandControl = commands.split('').filter(el => !el.match('[fblr]'));
      if (commandControl.length > 0) return 'Invalid command, allowed commads are fblr';
   
      this.visitedGrids.push(`${this.robot.x}, ${this.robot.y}`);
  
      // start robot
      for (let i = 0; i < commands.length; i++) {
        // Stop execution if robot is out of bounds
        if (this.outOfBounds) break;
  
        if(commands[i].match('[fb]') && this.checkObstacles(commands[i], this.robot, this.obstacles)) break
        commands[i].match('[lr]') && this.turn(commands[i]);
        commands[i].match('[fb]') && this.move(commands[i]);
      } 
      this.lastLocation = this.visitedGrids.pop()
      this.finalLocation.displayMessage('location', this.robot)
      return this.lastLocation;
    }

  
}

module.exports = Robot;