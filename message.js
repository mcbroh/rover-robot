class Message {
    constructor() {
        this.messages = {
            bounds: (position) => { throw new Error(`Out of bounds!, Location: ${position.x}, ${position.y}`) },
            obstacle: (position) => { throw new Error(`Total Stop!, You have hit an obstacle on location: ${position.x}, ${position.y}`) },
            location: (position) => { console.log(`Location!, You are at location x: ${position.x}, y: ${position.y}`) }
        }
    }

    displayMessage(messageType, position) {
       this.messages[messageType](position)
    }
}

module.exports = Message;