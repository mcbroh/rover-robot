const Grid = require('./grid');
const Message = require('./Message');

class ObstaclesChecker extends Grid {
    constructor() {
        super();
        this.message = new Message(null);
    }
    checkObstacles(direction, robot, obstacles) {
        // check out of bounds first
        const nextPos = this.nextGrid(direction, robot)
        //loop through obstacles aray to check next grid contains obstacles
        for (var j = 0; j < obstacles.x.length; j++) {
            if (nextPos.x === obstacles.x[j] && nextPos.y === obstacles.y[j]) {  
                this.message.displayMessage('obstacle', robot);              
            }
        }
        return false;
    }
}

module.exports = ObstaclesChecker;