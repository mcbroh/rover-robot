const Message = require('./Message');

class Surrounding extends Message {
    constructor(gridSize) {
        super()
        this.bounds = {
            N: (position, going) => going === 'f' ? position['y'] > 0 : position['y'] < gridSize,
            E: (position, going) => going === 'f' ? position['x'] < gridSize : position['x'] > 0,
            S: (position, going) => going === 'f' ? position['y'] < gridSize : position['y'] > 0,
            W: (position, going) => going === 'f' ? position['x'] > 0 : position['<'] < gridSize
        }
    }

    checkInBounds(position, facing) {  
        !this.bounds[position['direction'].toUpperCase()](position, facing) && this.displayMessage('bounds', position);       
        return this.bounds[position['direction'].toUpperCase()](position, facing);
    }
}

module.exports = Surrounding;