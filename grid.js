class Grid {
    nextGrid(direction, position) {        
      const pos = {...position},
      getGridPosition = {
        N: () => ({...pos, y: pos['y'] += direction === 'f' ? -1 : 1 }),
        E: () => ({...pos, x: pos['x'] += direction === 'f' ? 1 : -1 }),
        S: () => ({...pos, y: pos['y'] += direction === 'f' ? 1 : -1 }),
        W: () => ({...pos, x: pos['x'] += direction === 'f' ? -1 : 1 }),
      }
      
      return getGridPosition[position['direction'].toUpperCase()]();
    }
}

module.exports = Grid;