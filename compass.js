class Compass {
  constructor() {
    this._position = {  
      N: (face) => face === 'l' ? 'W' : 'E',
      E: (face) => face === 'l' ? 'N' : 'S',
      S: (face) => face === 'l' ? 'E' : 'W',
      W: (face) => face === 'l' ? 'S' : 'N'
    }
  }

  getNewDirection (oldDirection, turn) {
    return this._position[oldDirection](turn);
  }

}

module.exports = Compass;