const expect = require('chai').expect;
const Compass = require('../compass');
const Grid = require('../grid');
const ObstaclesChecker = require('../obstaclesChecker');
const Surrounding = require('../Surrounding');
const Robot = require('../index');


describe('Robot()', function () {

    // compass
    it('Test Class Compass.getNewDirection, left of North should equal W', function () {
        const compass = new Compass()
        const result = compass.getNewDirection('N', 'l')
        expect(result).to.be.equal('W');
    });

    it('Test Class Compass.getNewDirection, right of North should equal E', function () {
        const compass = new Compass()
        const result = compass.getNewDirection('N', 'r')
        expect(result).to.be.equal('E');
    });

    // Grid 
    it('Test Class Grid.nextGrid, should equal { x: 0, y: 1, direction: "N" }', function () {
        const grid = new Grid()
        const result = grid.nextGrid('N', {x: 0, y: 0, direction: 'N'})
        expect(result).to.be.eql({ x: 0, y: 1, direction: 'N' });
    });

    it('Test Class Grid.nextGrid, should equal { x: 0, y: 1, direction: "s" }', function () {
        const grid = new Grid()
        const result = grid.nextGrid('N', {x: 0, y: 0, direction: 's'})
        expect(result).to.be.eql({ x: 0, y: -1, direction: 's' });
    });

    // Obstacle
    it('Test Class ObstaclesChecker.checkObstacles, should throw error', function () {
        const obstaclesChecker = new ObstaclesChecker()        
        expect(() => obstaclesChecker.checkObstacles('f', {x: 0, y: 0, direction: 'E'}, { x: [1], y: [0] })).to.throw(Error, '0, 0');
    });

    // Surrounding
    it('Test Class Surrounding.checkInBounds, should return true', function () {
        const surrounding = new Surrounding(100)
        const result = surrounding.checkInBounds({x: 0, y: 0, direction: 'E'}, 'f')
        expect(result).to.be.equal(true);
    });

    it('Test Class Surrounding.checkInBounds, should throw error', function () {
        const surrounding = new Surrounding(100)
        expect(() => surrounding.checkInBounds({x: 0, y: 0, direction: 'e'}, 'b')).to.throw(Error, '0, 0');
    });

    // Invalid commands
    it('should return Invalid start, allowed commads are fblr', function () {
        const robot = new Robot(100, 'W', { x: 0, y: 0 })
        const result = robot.start('fk')
        expect(result).to.be.equal('Invalid command, allowed commads are fblr');
    });


    it('should stop at location 2, 2', function () {
        const robot = new Robot(100, 'S', { x: 0, y: 0 })
        const result = robot.start('fflff')
        expect(result).to.be.equal('2, 2');
    });
    it('should stop at location 1, 0 & throw error', function () {
        const robot = new Robot(50, 'N', { x: 1, y: 1 })
        expect(() => robot.start('fflff')).to.throw(Error, '1, 0');
    });

    // obstacles
    it('should stop at location 1, 0 & throw error', function () {
        const robot = new Robot(100, 'E', { x: 0, y: 0 }, { x: [2], y: [0] })
        expect(() => robot.start('fff')).to.throw(Error, '1, 0');
    });
    it('should stop at location 48, 49 & throw error', function () {
        const robot = new Robot(100, 'N', { x: 50, y: 50 }, { x: [48], y: [50] })
        expect(() => robot.start('fflffrbb')).to.throw(Error, '48, 49');
    });
});